/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* Provides userscript compatilibity with GreaseMonkey for Firefox
*
* Function definitions from the GPL book "Dive Into GreaseMonkey"
* ==> http://diveintogreasemonkey.org/api/
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*
Name
GM_log - log messages to the JavaScript Console

Synopsis
function GM_log(message);

Description
GM_log places output in the JavaScript Console. It is primarily used for 
debugging purposes.

History
GM_log was introduced in Greasemonkey 0.3. 
*/
function GM_log(message)
{
    
}

/*
Name
GM_getValue - get script-specific configuration value

Synopsis
returntype GM_getValue(key, defaultValue);

Description
GM_getValue retrieves a script-specific configuration value. The return value 
may be a string, boolean, or integer. The key argument is a string of no fixed 
format. The defaultValue argument is optional; if present, it will be returned 
if the requested key does not exist. If no defaultValue is given and the 
requested key does not exist, GM_getValue will return undefined.

Greasemonkey configuration values are similar to browser cookies, but there 
are important differences. Both are stored on the local machine, but while 
cookies are domain-specific and can only be accessed from their originating 
domain, Greasemonkey configuration values are script-specific and can only be 
accessed by the user script that created them (regardless of the address the 
user script is currently running on). And unlike cookies, configuration values 
are never transmitted to remote servers.

History
GM_getValue was introduced in Greasemonkey 0.3. 
*/
function GM_getValue(key, defaultValue)
{
    
}

/*
Name
GM_setValue - set script-specific configuration value

Synopsis
function GM_setValue(key, value);

Description
GM_setValue stores a script-specific configuration value. The key argument is 
a string of no fixed format. The value argument can be a string, boolean, or 
integer. Both arguments are required.

Greasemonkey configuration values are similar to browser cookies, but there 
are important differences. Both are stored on the local machine, but while 
cookies are domain-specific and can only be accessed from their originating 
domain, Greasemonkey configuration values are script-specific and can only 
be accessed by the user script that created them (regardless of the address 
the user script is currently running on). And unlike cookies, configuration 
values are never transmitted to remote servers.

History
GM_getValue was introduced in Greasemonkey 0.3. 
*/
function GM_setValue(key, value)
{
    
}

/*
Name
GM_registerMenuCommand - add a menu item to the User Script Commands submenu

Synopsis
function GM_registerMenuCommand(menuText, callbackFunction);

Description
GM_registerMenuCommand adds a menu item to the Tools => User Script Commands 
menu. The menuText argument is a string, the text of the menu item to add. 
The callbackFunction argument is a function object. When the menu item is 
selected, the callbackFunction function is called.

function callbackFunction(e);

The e parameter is the menu selection event. I'm not sure if this is useful 
for anything.

Bugs
There is no way to set a menu accelerator key. Calling 
GM_registerMenuCommand('Some &menu text', myFunction) will create a menu item 
titled "Some &menu text". (Bug 10090)

History
GM_registerMenuCommand was introduced in Greasemonkey 0.2.6.
*/
function GM_registerMenuCommand(menuText, callbackFunction)
{
    
}

/*
Name
GM_xmlhttpRequest - make an arbitrary HTTP request

Synopsis
GM_xmlhttpRequest(details);

Description
GM_xmlhttpRequest makes an arbitrary HTTP request. The details argument is an 
object that can contain up to seven fields.

method
    a string, the HTTP method to use on this request. Required. Generally GET, 
    but can be any HTTP verb, including POST, PUT, and DELETE. 
url
    a string, the URL to use on this request. Required.
headers
    an associative array of HTTP headers to include on this request. Optional, 
    defaults to an empty array. Example:

    headers: {'User-Agent': 'Mozilla/4.0 (compatible) Greasemonkey',
              'Accept': 'application/atom+xml,application/xml,text/xml'}

data
    a string, the body of the HTTP request. Optional, defaults to an empty 
    string. If you are simulating posting a form (method == 'POST'), you must 
    include a Content-type of 'application/x-www-form-urlencoded' in the 
    headers field, and include the URL-encoded form data in the data field. 
onload
    a function object, the callback function to be called when the request 
    has finished successfully.
onerror
    a function object, the callback function to be called if an error occurs 
    while processing the request.
onreadystatechange
    a function object, the callback function to be called repeatedly while the 
    request is in progress.

onload callback

The callback function for onload takes a single parameter, responseDetails.

function onloadCallback(responseDetails);

responseDetails is an object with five fields.

status
    an integer, the HTTP status code of the response. 200 means the request 
    completed normally. 
statusText
    a string, the HTTP status text. Status text is server-dependent. 
responseHeaders
    a string, the HTTP headers included in the response. 
responseText
    a string, the body of the response.
readyState
    unused

onerror callback

The callback function for onerror takes a single parameter, responseDetails.

function onerrorCallback(responseDetails);

responseDetails is an object with five fields.

status
    an integer, the HTTP error code of the response. 404 means the page was 
    not found. 
statusText
    a string, the HTTP status text. Status text is server-dependent. 
responseHeaders
    a string, the HTTP headers included in the response. 
responseText
    a string, the body of the response. The body of an HTTP error page is 
    server-dependent. 
readyState
    unused

onreadystatechange callback

The callback function for onreadystatechange is called repeatedly while the 
request is in progress. It takes a single parameter, responseDetails.

function onreadystatechangeCallback(responseDetails);

responseDetails is an object with five fields. The responseDetails.readyState 
denotes what stage the request is currently in.

status
    an integer, the HTTP status code of the response. This will be 0 when 
    responseDetails.readyState < 4. 
statusText
    a string, the HTTP status text. This will be an empty string when 
    responseDetails.readyState < 4. 
responseHeaders
    a string, the HTTP headers included in the response. This will be an empty 
    string when responseDetails.readyState < 4. 
responseText
    a string, the body of the response. This will be an empty string when 
    responseDetails.readyState < 4. 
readyState
    an integer, the stage of the HTTP request.

    1
        loading. The request is being prepared.
    2
        loaded. The request is ready to be sent to the server, but nothing 
        has been sent yet.
    3
        interactive. The request has been sent and the client is waiting for 
        the server to finish sending data.
    4
        complete. The request is completed and all response data is available 
        in other fields.

Examples
The following code fetches the Atom feed from http://greaseblog.blogspot.com/ 
and displays an alert with the results.

GM_xmlhttpRequest({
    method: 'GET',
    url: 'http://greaseblog.blogspot.com/atom.xml',
    headers: {
        'User-agent': 'Mozilla/4.0 (compatible) Greasemonkey',
        'Accept': 'application/atom+xml,application/xml,text/xml',
    },
    onload: function(responseDetails) {
        alert('Request for Atom feed returned ' + responseDetails.status +
              ' ' + responseDetails.statusText + '\n\n' +
              'Feed data:\n' + responseDetails.responseText);
    }
});

Bugs
The onreadystatechange callback does not work properly with readyState < 4.


Notes
Unlike the XMLHttpRequest object, GM_xmlhttpRequest is not restricted to the 
current domain; it can GET or POST data from any URL.

History
GM_xmlhttpRequest was introduced in Greasemonkey 0.2.6. 
*/
function GM_xmlhttpRequest(details)
{
    
}

