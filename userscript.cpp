/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "userscript.h"

#include <qwidget.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qlist.h>
#include <qfileinfo.h>
#include <qfile.h>
#include <qdir.h>
#include <qtextstream.h>
#include <qregexp.h>


#include <kconfig.h>
#include <kconfiggroup.h>
#include <kdebug.h>
#include <kstandarddirs.h>

/*
*
*/
UserScript::UserScript(const QFileInfo &fileInfo)
{
     m_filepath = fileInfo.filePath();
     m_filename = fileInfo.fileName();

     readInfo();
}

/*
*
*/
UserScript::UserScript(const QString &url)
{
     QFileInfo fileInfo = QFileInfo(url);

     m_filepath = fileInfo.filePath();
     m_filename = fileInfo.fileName();

     readInfo();
}

/*
*
*/
UserScript::~UserScript()
{

}

/*
*
*/
const QString UserScript::filename()
{
    return m_filename;
}

/*
*
*/
const QString UserScript::filepath()
{
    return m_filepath;
}

/*
*
*/
const QString UserScript::name()
{
    return m_name;
}

/*
*
*/
const QString UserScript::version()
{
    return m_version;
}

/*
*
*/
const QString UserScript::ns()
{
    return m_ns;
}

/*
*
*/
const QString UserScript::author()
{
    return m_author;
}

/*
*
*/
const QString UserScript::description()
{
    return m_description;
}

/*
*
*/
const QStringList UserScript::includes()
{
    return m_includes;
}

/*
*
*/
const QStringList UserScript::excludes()
{
    return m_excludes;
}

/*
*
*/
const QString UserScript::script()
{
    return m_script;
}

/*
*
*/
bool UserScript::enabled() const
{
    return m_enabled;
}

/*
* This function needs to be rewritten:
- Do no read the entire file (only the header)
- No need for so many regexps
*/
bool UserScript::readInfo()
{
    // Defaults to enabled.
    m_enabled = true;

    QFile file(m_filepath);

    if (!file.open(QIODevice::ReadOnly)) {
        kError() << "Could not open file " << m_filename << "\n";
        return false;
    }

    // Let's read the content of the file
    QTextStream stream(&file);
    QString line = "\n";

    //TODO why can't I read the file content
    while (! (stream.atEnd())) {
        line += (stream.readLine() + "\n");
    }
    file.close();

    QRegExp re;
    re.setMinimal(true);

// ==UserScript==
// @name		Plugin Name
// @version		x.x+
// @namespace		http://greasemonkey.example.com
// @author		John Smith
// @description		Provides an example header for userscripts
// @include		*
// @exclude		*.example.com*
// @exclude		*.test.com/*
// ==/UserScript==

    re.setPattern("//\\s+==UserScript==(.*)//\\s+==/UserScript==\\s*\n");
    if(re.indexIn(line)) {

        QString scriptinfo = re.cap(1);
        kDebug() << "Script info: " << scriptinfo << "\n";

        re.setPattern("//\\s+@name\\s+(.*)\\s*\n");
        if(re.indexIn(scriptinfo)) {
            m_name = re.cap(1).trimmed();
        }

        re.setPattern("//\\s+@version\\s+(.*)\\s*\n");
        if(re.indexIn(scriptinfo)) {
            m_version = re.cap(1).trimmed();
        }

        re.setPattern("//\\s+@namespace\\s+(.*)\\s*\n");
        if(re.indexIn(scriptinfo)) {
            m_ns = re.cap(1).trimmed();
        }

        re.setPattern("//\\s+@author\\s+(.*)\\s*\n");
        if(re.indexIn(scriptinfo)) {
            m_author = re.cap(1).trimmed();
        }

        re.setPattern("//\\s+@description\\s+(.*)\\s*\n");
        if(re.indexIn(scriptinfo)) {
            m_description = re.cap(1).trimmed();
        }

        re.setPattern("//\\s+@include\\s+(.*)\\s*\n");
        int pos = 0;
        while((pos = re.indexIn(scriptinfo, pos)) != -1) {
            m_includes.append(re.cap(1).trimmed());
            pos+=re.matchedLength();
        }

        re.setPattern("//\\s+@exclude\\s+(.*)\n");
        pos = 0;
        while((pos = re.indexIn(scriptinfo, pos)) != -1) {
            m_excludes.append(re.cap(1).trimmed());
            pos+=re.matchedLength();
        }

    }
    else {
    	kDebug() << "File " << m_filename << "does not appear to be a valid userscript\n";
        return false;
    }

    m_script = line;

    return true;
}

/*
*
*/
bool UserScript::matchUrl(const QString & url)
{
    QRegExp re;

    re.setPatternSyntax (QRegExp::Wildcard);
    //re.setWildcard(true);

    bool included = false;
    bool excluded = false;

    for(QStringList::Iterator inc_it = m_includes.begin(); inc_it != m_includes.end(); ++inc_it) {
        re.setPattern(*inc_it);
        if (re.exactMatch(url)) {
            included = true;
            break;
        }
    }

    for(QStringList::Iterator exc_it = m_excludes.begin(); exc_it != m_excludes.end(); ++exc_it) {
        re.setPattern(*exc_it);
        if (re.exactMatch(url)) {
            excluded = true;
            break;
        }
    }

    if(included && !excluded) {
         kDebug() << "Userscript [" << m_filename << "] successfully met url \"" << url << "\" include/exclude rules\n";
        return true;
    }
    else {
        kDebug() << "Userscript [" << m_filename << "] did not meet url \"" << url << "\"include/exclude rules\n";
        return false;
    }
}

/*
*
*/
bool UserScript::isValid()
{
    //TODO: Write me!
    return true;
}

/*
*
*/
void UserScript::slotEnable(bool chkValue)
{
    m_enabled = chkValue;

    KConfig* kconfig = new KConfig("khtml_userscriptrc");
    KConfigGroup grp(kconfig, m_filename);
    grp.writeEntry("enabled", chkValue);
    kconfig->sync();
    delete kconfig;
}

#include "userscript.moc"

