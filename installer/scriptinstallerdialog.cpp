/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#include "scriptinstallerdialog.h"

#include "../userscript.h"
#include "../userscriptlist.h"

#include <QFile>
#include <KDebug>
#include <KMessageBox>
#include <KStandardDirs>

/*
*
*/
ScriptInstallerDialog::ScriptInstallerDialog(UserScript* script)
{
    m_script = script;
    m_ui.setupUi(this);

    initScript();

    // Connect save button
    // Connect close button
    connect(m_ui.buttonBox, SIGNAL(accepted()), this, SLOT(installScript()));
    connect(m_ui.buttonBox, SIGNAL(rejected()), this, SLOT(close()));
}

/*
*
*/
ScriptInstallerDialog::~ScriptInstallerDialog()
{

}


/*
*
*/
void ScriptInstallerDialog::initScript()
{
    m_ui.nameLabel->setText(m_script->name() + i18n( " v." ) + m_script->version());
    m_ui.descriptionLabel->setText(m_script->description());
    m_ui.authorLabel->setText(m_script->author());
    m_ui.scriptContent->setPlainText(m_script->script());
}

/*
*
*/
bool ScriptInstallerDialog::installScript()
{
    kDebug() << KStandardDirs::locateLocal("data", "konqueror/userscripts/", true) << m_script->filename();

    //bool saved = false;
    bool saved = QFile::copy(m_script->filepath(), KStandardDirs::locateLocal("data", "konqueror/userscripts/", true) + m_script->filename());

    close();

    if(saved) {
        KMessageBox::information(0, i18n("Script successfully installed. You will need to restart Konqueror"), i18n("Userscript installed"));
        QList<UserScript*> usl = UserScriptList::scriptList();
        usl << m_script;
	return true;
    }
    else {
        KMessageBox::error(0, i18n("Script could not be saved. Please make sure a script with the same name is not already installed"), i18n("Could not install Userscript"));
	return false;
    }
    return saved;
}

#include "scriptinstallerdialog.moc"
