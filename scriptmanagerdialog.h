/*
* Copyright (c) 2007 Mathieu Ducharme <ducharme.mathieu@gmail.com>
*
* See AUTHORS for details
*
* This software is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This software is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; see the file COPYING.
* If not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

#ifndef _SCRIPTMANAGER_H_
#define _SCRIPTMANAGER_H_

#include <QWidget>

#include "userscript.h"

#include "ui_scriptmanagerdialog.h"


class ScriptManager : public QWidget, public Ui::ScriptManager
{
public:
  ScriptManager( QWidget *parent ) : QWidget( parent ) {
    setupUi( this );
  }
};


/**
*
*/
class ScriptManagerDialog : public KDialog
{
Q_OBJECT
public:

    /**
    *
    */
    ScriptManagerDialog();

    /**
    *
    */
    virtual ~ScriptManagerDialog();

    /**
    * Initialization
    */
    void init();


public slots:

    /**
    *
    */
    void selectScript(int pos);

private:

    /**
    *
    */
    ScriptManager *m_ui;

};

#endif // _SCRIPTMANAGER_H_
