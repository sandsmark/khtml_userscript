// ==UserScript==
// @name		KHTML Userscript (kuskus) Test
// @version		0.1.1
// @namespace           http://www.kde.org
// @author		Akhmad Fathonih
// @description	        Change links in red, only for testing
// @include		*.kde.org*
// @exclude		*.google.com*
// ==/UserScript==

var elms = document.getElementsByTagName("a");
//alert(elms.length);

for(var i=0;i<elms.length;i++) {
	elms[i].style.color="red";
	elms[i].style.border="1px solid";
}
